﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace PCAcompression {
	class Program {

		//	Набор математических операций, необходимых для работы алгоритма
		private static class Math {

			//	Модуль числа
			public static double Abs(double Number) {
				return (Number > 0 ? Number : -Number);
			}

			//	Квадрат числа
			public static double Sqr(double Number) {
				return Number * Number;
			}

			//	Квадратный корень.
			public static double Sqrt(double Number) {
				return System.Math.Sqrt(Number);
			}

			//	Среднее значение
			public static double Mean(double[] Array) {
				double mean = 0;
				foreach (double Number in Array) {
					mean += Number;
				}
				return mean / Array.Length;
			}

			//	Стандартное отклонение
			public static double StandardDeviation(double[] Array) {
				return Sqrt(Variance(Array));
			}

			//	Дисперсия
			public static double Variance(double[] Array) {
				return Covariance(Array, Array);
			}

			//	Ковариантность
			public static double Covariance(double[] Data1, double[] Data2) {
				double N1 = Data1.Length,
							 N2 = Data2.Length,
							 mean1 = Mean(Data1),
							 mean2 = Mean(Data2);

				if (N1 != N2) {
					throw new ArgumentException("N1 != N2");
				}

				double covariance = 0;
				for (int i = 0; i < Data1.Length; i++) {
					covariance += (Data1[i] - mean1) * (Data2[i] - mean2);
				}
				return covariance / (N1 - 1);
			}

			//	Матрица ковариантности
			public static double[][] Covariance(double[][] Data) {
				int N1 = Data.Length,
						N2 = Data[0].Length;

				double[][] covariance = new double[N1][];

				for (int i = 0; i < N1; i++) {
					covariance[i] = new double[N1];
					for (int j = 0; j < N1; j++) {
						//	Здесь есть место для оптимизации
						covariance[i][j] = Covariance(Data[i], Data[j]);
					}
				}

				return covariance;
			}
		}

		//	Матрица
		public class Matrix {
			public double[][] data;
			public int Width, Height;

			public Matrix(double[][] Data) {
				data = Data;
				Height = Data.Length;
				Width = Data[0].Length;
			}

			public Matrix(int height, int width) {
				data = new double[height][];
				for (int i = 0; i < height; i++)
					data[i] = new double[width];
				Width = width;
				Height = height;
			}

			//	Обращение к элементу матрицы
			public double[] this[int X] {
				get { return data[X]; }
				set { data[X] = value; }
			}

			//	Умножение матриц
			public static Matrix operator *(Matrix left, Matrix right) {
				if (left.Width != right.Height)
					throw new Exception("Left.width != right.height");

				int height = left.Height;
				int width = right.Width;
				double[][] resultMatrix = new double[height][];

				for (int i = 0; i < height; i++)
					resultMatrix[i] = new double[width];

				for (int i = 0; i < height; i++)
					for (int j = 0; j < width; j++)
						for (int k = 0; k < left.Width; k++)
							resultMatrix[i][j] += left[i][k] * right[k][j];

				return new Matrix(resultMatrix);
			}

			//	Транспонирование матрицы
			public Matrix Transponse() {
				Matrix result = new Matrix(Width, Height);
				for (int i = 0; i < Height; i++) 
					for (int j = 0; j < Width; j++) {
						result.data[j][i] = data[i][j];
					}
				return result;
			}

			//	Вывод матрицы
			public void PrintMatrix(StreamWriter sw) {
				foreach (double[] row in data) {
					foreach (double number in row) {
						sw.Write("{0:f4} ", number);
					}
					sw.WriteLine();
				}
			}

			public void PrintMatrix() {
				foreach (double[] row in data) {
					foreach (double number in row) {
						Console.Write("{0:f4} ", number);
					}
					Console.WriteLine();
				}
			}

      //  Привод к диагональному виду
			public void MakeDiagonal() {
				for (int i = 0; i < Height; i++)
					for (int j = 0; j < Width; j++)
						data[i][j] = (i == j ? 1 : 0);
			}

			//	Уменьшает до ширины newWidth первых колонок
			public Matrix Reduce (int newWidth) {
				Matrix result = new Matrix(newWidth, Height);
				Matrix tmp = this.Transponse();
				for (int i = 0; i < newWidth; i++)
					result[i] = tmp[i];
				return result.Transponse();
			}
		}

		//	Класс для вычисления собственных чисел матрицы
		public static class Eigen {
			public static void CalculateEigen(Matrix matrix, int iterationsMax, ref Matrix vectors, ref double[] values) {
				if (matrix.Height != matrix.Width)
					throw new Exception("Matrix is not square");
				int N = matrix.Height;

				vectors = new Matrix(N, N);
				vectors.MakeDiagonal();
				values = GetDiagonal(matrix);

				double[] bw = new double[N],
								 zw = new double[N];

				for (int i = 0; i < N; i++) {
					bw[i] = values[i];
					zw[i] = 0;
				}

				int it_num = 0,
						rot_num = 0;

				while (it_num < iterationsMax) {
					it_num++;

					double thresh = 0;
					for (int i = 0; i < N; i++)
						for (int j = 0; j < i; j++)
							thresh += Math.Sqr(matrix[i][j]);

					thresh = Math.Sqrt(thresh) / ((double) 4 * N);

					if (thresh == 0)
						break;

					for (int p = 0; p < N; p++)
						for (int q = p + 1; q < N; q++) {
							double gapq = 10.0 * Math.Abs(matrix[q][p]);
							double termp = gapq + Math.Abs(values[p]);
							double termq = gapq + Math.Abs(values[q]);

							if (4 < it_num &&
									termp == Math.Abs(values[p]) &&
									termq == Math.Abs(values[q])) {
								matrix[q][p] = 0;
							} else if (thresh <= Math.Abs(matrix[q][p])) {
								double h = values[q] - values[p];
								double term = Math.Abs(h) + gapq;
								double t, theta;

								if (term == Math.Abs(h))
									t = matrix[q][p] / h; 
								else {
									theta = .5 * h / matrix[q][p];
									t = 1.0 / (Math.Abs(theta) + Math.Sqrt(1.0 + Math.Sqr(theta)));
									if (theta < 0)
										t = -t;
								}

								double c = 1.0 / Math.Sqrt(1.0 + t * t);
								double s = t * c;
								double tau = s / (1.0 + c);
								h = t * matrix[q][p];

								zw[p] -= h;
								zw[q] += h;
								values[p] -= h;
								values[q] += h;

								matrix[q][p] = 0;

								for (int j = 0; j < p; j++) {
									double g1 = matrix[p][j];
									double g2 = matrix[q][j];
									matrix[p][j] = g1 - s * (g2 + g1 * tau);
									matrix[q][j] = g2 + s * (g1 - g2 * tau);
								}

								for (int j = p + 1; j < q; j++) {
									double g1 = matrix[j][p];
									double g2 = matrix[q][j];
									matrix[j][p] = g1 - s * (g2 + g1 * tau);
									matrix[q][j] = g2 + s * (g1 - g2 * tau);
								}

								for (int j = q + 1; j < N; j++) {
									double g1 = matrix[j][p];
									double g2 = matrix[j][q];
									matrix[j][p] = g1 - s * (g2 + g1 * tau);
									matrix[j][q] = g2 + s * (g1 - g2 * tau);
								}

								for (int j = 0; j < N; j++) {
									double g1 = vectors[p][j];
									double g2 = vectors[q][j];
									vectors[p][j] = g1 - s * (g2 + g1 * tau);
									vectors[q][j] = g2 + s * (g1 - g2 * tau);
								}
								rot_num++;
							}
						}

					for (int i = 0; i < N; i++) {
						bw[i] += zw[i];
						values[i] = bw[i];
						zw[i] = 0;
					}
				}

				for (int j = 0; j < N; j++)
					for (int i = 0; i < j; i++)
						matrix[j][i] = matrix[i][j];

				for (int k = 0; k < N - 1; k++) {
					int m = k;
					for (int l = k + 1; l < N; l++)
						if (values[l] > values[m])
							m = l;

					if (m != k) {
						double t = values[m];
						values[m] = values[k];
						values[k] = t;
						double[] tmp = vectors[m];
						vectors[m] = vectors[k];
						vectors[k] = tmp;
					}
				}
			}

      //  Возвращает все диагональные элементы матрицы в массив
			private static double[] GetDiagonal(Matrix m) {
				double[] result = new double[m.Height];
				for (int i = 0; i < m.Height; i++)
					result[i] = m[i][i];
				return result;
			}
		}

		//	Перевод строки в число
		public static double StoD(string s) {
			return double.Parse(s.Replace('.', ','));
		}

		//	Получение средних значений в каждом измерении (читая их из потока)
		public static double[] GetMeans(StreamReader sr, int Samples, int Dimensions) {
			double[] means = new double[Dimensions];

			for (int i = 0; i < Samples; i++) {
				//	По-другому считывать числа может быстрее?
				string line = sr.ReadLine();
				string[] numbers = line.Split(' ');

				for (int j = 0; j < Dimensions; j++) {
					means[j] += StoD(numbers[j]);
				}
			}

			for (int i = 0; i < Dimensions; i++)
				means[i] /= Samples;

			return means;
		}

		//	Получение матрицы ковариантности из потока
		public static double[][] GetCovarianceMatrix(StreamReader sr, int Samples, int Dimensions, double[] Means) {
			double[][] cm = new double[Dimensions][];
			for (int i = 0; i < Dimensions; i++)
				cm[i] = new double[Dimensions];

			//	Пропускаем первые две строчки (можно как-то изменить)
			sr.ReadLine(); sr.ReadLine();

			for (int i = 0; i < Samples; i++) {
				string line = sr.ReadLine();
				string[] Snumbers = line.Split(' ');
				double[] numbers = new double[Dimensions];
				for (int j = 0; j < Dimensions; j++)
					numbers[j] = StoD(Snumbers[j]) - Means[j];

				for (int j = 0; j < Dimensions; j++)
					for (int k = j; k < Dimensions; k++)
						cm[j][k] += numbers[j] * numbers[k];
			}

			for (int i = 0; i < Dimensions; i++)
				for (int j = i; j < Dimensions; j++)
					cm[j][i] = cm[i][j] /= Samples - 1;
			return cm;
		}

    //  Вывод данных
		public static class Logger {
			public static void Log(string message) {
				Console.Write(message);
			}

			public static void Log(double[] array, string message) {
				Console.Write(message);
				for (int i = 0; i < array.Length; i++)
					Console.Write("{0:f4}" + (i == array.Length - 1 ? "\n" : " "), array[i]);
			}

			public static void Log(double[][] array, string message) {
				Console.WriteLine(message);
				for (int i = 0; i < array.Length; i++)
					Log(array[i], "");
			}
		}

		//	Сокращение измерений
		public static void AdjustValues(StreamReader sr, Matrix ev, double[] means, int reduceTo, StreamWriter sw) {

			int Samples = int.Parse(sr.ReadLine());
			int Dimensions = int.Parse(sr.ReadLine());

			Matrix evReduced = ev.Reduce(reduceTo);

			for (int i = 0; i < Samples; i++) {
				string[] values = sr.ReadLine().Split(' ');
				double[] numbers = new double[Dimensions];
				for (int j = 0; j < Dimensions; j++)
					numbers[j] = StoD(values[j]) - means[j];

				Matrix row = new Matrix(new double[][] { numbers });
				(evReduced.Transponse() * row.Transponse()).Transponse().PrintMatrix(sw);
			}
		}

		static void Main(string[] args) {

			//	Входные данные:
			//	Первая строка - количество образцов
			//	Второая строка - количество измерений
			//	Файл по умолчанию - input.txt
      //  Выходные данные:
      //  Первая строка - количество образцов
      //  Вторая строка - количество измерений

			StreamReader file;
			string inputFilePath = "input.txt";
			string outputFilePath = "output.txt";

			if (args.Length > 0)
				inputFilePath = args[0];
			if (args.Length > 1)
				outputFilePath = args[1];

			file = new StreamReader(inputFilePath);

			int Samples = int.Parse(file.ReadLine()),
				Dimensions = int.Parse(file.ReadLine());

			double[] Means = GetMeans(file, Samples, Dimensions);
      if (Dimensions <= 10) {
        Logger.Log(Means, "Means:\n");
        Logger.Log("\n");
      }
			file.Close();

			file = new StreamReader(inputFilePath);
			double[][] CovarianceMatrix = GetCovarianceMatrix(file, Samples, Dimensions, Means);
      if (Dimensions <= 10) {
        Logger.Log(CovarianceMatrix, "Covariance Matrix:");
        Logger.Log("\n");
      }
			file.Close();

			Matrix EigenVectors = new Matrix(Dimensions, Dimensions);
			double[] EigenValues = new double[Dimensions];

			Eigen.CalculateEigen(new Matrix(CovarianceMatrix), (int) 1e9, ref EigenVectors, ref EigenValues);
      if (Dimensions <= 10) {
        Logger.Log("EigenVectors:\n");
        EigenVectors.Transponse().PrintMatrix();
        Logger.Log("\n");
      }

			file = new StreamReader(inputFilePath);
      Console.WriteLine("Сохранение информации: введите процент информации, который нужно сохранить");
      double dataToSave = double.Parse(Console.ReadLine());
      double eigenSumm = 0;
      foreach (double value in EigenValues)
        eigenSumm += value;
      double summ = 0;
      for (int i = 0; i < EigenValues.Length; i++) {
        summ += EigenValues[i];
        double dataSaved = 100 * summ / eigenSumm;
        Console.WriteLine("{1} компонент = {0:f4}%", dataSaved, i + 1);
        if (dataSaved > dataToSave)
          break;
      }

      Console.WriteLine("Введите измерение до какого понизить: от 1 до {0}", Dimensions);
			int reduceTo = int.Parse(Console.ReadLine());

      //Logger.Log("Adjust data:\n");
      StreamWriter sw = new StreamWriter(outputFilePath);
			sw.WriteLine(Samples);
			sw.WriteLine(reduceTo);

			AdjustValues(file, EigenVectors, Means, reduceTo, sw);
      file.Close();
			sw.Close();
		}
	}
}